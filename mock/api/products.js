module.exports = {
  success: true,
  data: [
    {
      id: '1',
      name: 'iPhone 7',
      status: 'onsale',
      price: '$749.00',
      img: '/static/phones/iphone7.png'
    }, {
      id: '2',
      name: 'iPhone 7 Plus',
      status: 'soldout',
      price: '$849.00',
      img: '/static/phones/iphone7plus.png'
    }, {
      id: '3',
      name: 'Samsung S6',
      status: 'onsale',
      price: '$570.00',
      img: '/static/phones/samsungs6.png'
    }, {
      id: '4',
      name: 'Samsung S7',
      status: 'onsale',
      price: '$670.00',
      img: '/static/phones/samsungs7.png'
    },
    {
      id: '5',
      name: 'iPhone 6',
      status: 'onsale',
      price: '$549.00',
      img: '/static/phones/iphone6.png'
    }, {
      id: '6',
      name: 'iPhone 5',
      status: 'soldout',
      price: '$349.00',
      img: '/static/phones/iphone5.png'
    }, {
      id: '7',
      name: 'Huawei P9',
      status: 'onsale',
      price: '$599.00',
      img: '/static/phones/huaweip9.png'
    }, {
      id: '8',
      name: 'Xiaomi 6',
      status: 'onsale',
      price: '$299.00',
      img: '/static/phones/xiaomi6.png'
    },
    {
      id: '9',
      name: 'Vivo X7',
      status: 'onsale',
      price: '$499.00',
      img: '/static/phones/iphone7.png'
    }, {
      id: '10',
      name: 'Moto X3',
      status: 'onsale',
      price: '$599.00',
      img: '/static/phones/iphone7.png'
    }
  ]
}
